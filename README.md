# React Redux Material Menu

[tinyurl.com/yd4yb4jj](https://tinyurl.com/yd4yb4jj)

Use Material UI to create a twin menu for PWAs

## Install & Start

```bash
cd <working-directory>
git clone https://bitbucket.org/listingslab/react-redux-material-menu
cd react-redux-material-menu
npm install
npm start
```

## Setup Firebase

- Log into the [Firebase Console](https://console.firebase.google.com/) and create a new project. Make a note of the Project ID.
- Edit `/deploy/.firebaserc`, replacing `"default": "react-redux-material-menu"` with your newly created Project ID
- Install and log into the [Firebase CLI tools](https://firebase.google.com/docs/cli/)
- In a terminal change to the `deploy` directory
- run `firebase deploy` to test your Firebase setup.

## Gulp deployment

The project can be easily deployed using the `npm run deploy` command which runs the `gulpfile.js`. The sequence of gulp processes first cleans out the `deploy/build` folder, then runs the create-react-app build process, copies the folder to `deploy/build` and then runs `firebase deploy` to transfer the build files to Firebase.
