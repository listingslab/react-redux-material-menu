/**
 * Skool
 * Jan 2018
 */

const gulp = require('gulp');
const clean = require('gulp-clean');
const gulpSequence = require('gulp-sequence');
const file = require('gulp-file');
const shell = require('gulp-shell');
const color = require('gulp-color');
const package = require('./package.json');

let purgeRequired = true;

gulp.task('default', () => {
  console.log (color('\nDeploying ' + package.name + ' ' + package.version + ' to Firebase', 'GREEN'));
  gulp.start('start-deploy');
});

gulp.task('start-deploy', gulpSequence('clean', 'build', 'copy', 'version', 'firebase', 'complete-deploy'));

gulp.task('clean', () => {
  console.log (color('Cleaning deploy/build/', 'YELLOW'));
  gulp.src(['deploy/build/*'], {read:false})
  .pipe(clean());
});

gulp.task('build', () => {
  console.log (color('Running npm run build', 'YELLOW'));
  return gulp.src('*.js')
    .pipe(shell(['npm run build']))
});

gulp.task('copy', () => {
  console.log (color('Copying build folder', 'YELLOW'));
  return gulp.src('build/**/*')
    .pipe(gulp.dest('./deploy/build'));
});

gulp.task('version', () => {
  console.log (color('Creating version.json', 'YELLOW'));
  versionJSON = `{
  "version": "${package.version}",
  "purgeRequired": ${purgeRequired}
}`;
  file('version.json', versionJSON, { src: true })
    .pipe(gulp.dest('./deploy/build'));
});

gulp.task('firebase', () => {
  console.log (color('Deploying to firebase', 'YELLOW'));
  return gulp.src('*.js', {read: false})
    .pipe(shell(['cd ./deploy && firebase deploy && clear']))
});

gulp.task('complete-deploy', () => {
  console.log (color('\nSuccessfully Deployed ' + package.name + ' ' + package.version + ' to Firebase', 'GREEN'));
  console.log (color(package.firebaseURL, 'WHITE'));
  console.log ('\n');
});

const arg = (argList => {
  let arg = {}, a, opt, thisOpt, curOpt;
  for (a = 0; a < argList.length; a++) {
    thisOpt = argList[a].trim();
    opt = thisOpt.replace(/^\-+/, '');
    if (opt === thisOpt) {
      if (curOpt) arg[curOpt] = opt;
      curOpt = null;
    }
    else {
      curOpt = opt;
      arg[curOpt] = true;
    }
  }
  return arg;
})(process.argv);