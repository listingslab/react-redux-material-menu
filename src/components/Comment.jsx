/**
 * Skool
 * Jan 2018
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import withRoot from '../styles/withRoot';
import classNames from 'classnames';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';

const styles = theme => ({
  comment: {
    marginBottom: theme.spacing.unit * 2,
  },
  commentInner: {
    position: 'relative',
  },
  bigAvatar: {
    border: '5px solid white',
    width: 75,
    height: 75,
  },
  commentTitle:{
    background: 'black',
    width: 'calc(100% - 23px)',
    height: 50,
    position: 'absolute',
    top: 20,
    left: 25
  },
  commentTitleText:{
    paddingLeft: 80,
    paddingTop: 15,
    color:'white',
    fontSize: '1.1rem',
  },
  commentBody:{
    border: '1px solid black',
    background: 'white',
    width: 'calc(100% - 75px)',
    marginLeft: 45,
    marginTop: -15,
    padding: 15
  }
});

const Comment = (props) => {
  const { classes } = props;
  return (
    <div className={classNames(classes.comment)} >
      <div className={classNames(classes.commentInner)} >
        <div className={classNames(classes.commentTitle)}>
          <Typography
            className={classNames(classes.commentTitleText)}>
            Bob Says
          </Typography>
      </div>
        <Avatar
          alt="Adelle Charles"
          src="jpg/uxceo-128.jpg"
          className={classNames(classes.avatar, classes.bigAvatar)}
        />
        <div className={classNames(classes.commentBody)}>
          <Typography
            className={classNames(classes.colTitle)}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. In augue erat, laoreet et dolor vulputate, lobortis tincidunt sapien. Proin a ipsum ligula. Mauris ac nunc vel felis eleifend accumsan. Ut ut nisi eu odio interdum bibendum in sed mi. Ut quis malesuada risus. Ut molestie ante in ex feugiat, et
          </Typography>
        </div>
      </div>
    </div>
  );
}

Comment.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(Comment));
