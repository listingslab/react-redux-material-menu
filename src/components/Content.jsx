/**
 * Skool
 * Jan 2018
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import withRoot from '../styles/withRoot';
import classNames from 'classnames';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import SimpleMediaCard from './Content/SimpleMediaCard';
import SwitchListSecondary from './Content/SwitchListSecondary';
import Comment from './Comment';

const styles = theme => ({
  content: {
    marginBottom: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 3,
    height: '100%',
  },
});

const Content = (props) => {
  const { classes } = props;
  return (
    <div className={classNames(classes.content)} >

      <Grid
        container
        spacing={16}
        direction="row"
        justify="flex-start"
        alignItems="flex-start"
        className={classNames()}>

        <Grid
          item
          xl={8} lg={8} md={8} sm={12} xs={12}
          className={classNames()}>
          <Comment />
          <Comment />
        </Grid>

        <Grid
          item
          xl={4} lg={4} md={4} sm={12} xs={12}
          className={classNames()}>
          <SimpleMediaCard />
          <Paper className={classes.paper} elevation={4}>
            <SwitchListSecondary />
          </Paper>
        </Grid>

      </Grid>

    </div>
  );
}

Content.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(Content));

/*
 direction="row || row-reverse || column || column-reverse"
 justify="flex-start || center || flex-end || space-between || space-around"
 alignItems="flex-start || center || flex-end || stretch || baseline"
 */