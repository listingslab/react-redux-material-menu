/**
 * Skool
 * Jan 2018
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import ExpansionPanel, {
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';

const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

function SimpleExpansionPanel(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <ExpansionPanel defaultExpanded={true}>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>Expansion Panel 1</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
            Fusce porttitor facilisis ex, eu ultrices est pellentesque quis. Mauris volutpat velit bibendum turpis sagittis accumsan. Nunc risus dui, pulvinar nec ullamcorper vel, pulvinar et quam. Sed nulla ligula, facilisis ultrices lectus in, commodo congue ligula. Curabitur at fringilla nisi. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris tincidunt viverra mi a semper. Ut fringilla erat neque, eget sodales sapien mattis a. Donec viverra sit amet sem id congue.
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel>
        <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
          <Typography className={classes.heading}>Expansion Panel 2</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>
            Fusce vel faucibus arcu, a faucibus magna. Maecenas sed erat luctus, vestibulum justo id, tincidunt arcu. Quisque odio metus, egestas at volutpat ut, porttitor sed risus. Sed sit amet auctor lectus, eu sodales sapien. Nam congue erat urna, vitae consequat sem maximus id. Integer sed odio ex. Sed luctus lobortis elit, et accumsan neque ullamcorper elementum. Morbi eu viverra velit. Phasellus feugiat est nisl, fringilla pharetra mauris facilisis id. Maecenas felis sapien, porttitor ac ultricies et, ultrices sed ex. Nullam quis massa eros. Duis eget dui vitae tortor elementum egestas. Ut ac elit tempor, egestas massa at, dictum tortor. Integer rutrum quam dictum nulla suscipit, nec sodales odio bibendum. Aliquam non velit ornare, maximus justo non, feugiat ex.
          </Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
}

SimpleExpansionPanel.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleExpansionPanel);