/**
 * Skool
 * Jan 2018
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import withRoot from '../styles/withRoot';
import classNames from 'classnames';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';

const styles = theme => ({
  footer: {
    marginTop: theme.spacing.unit*2,
  },
  bordered: {
    border: '1px solid white',
    padding: 20
  },
  colTitle: {
    color: 'white',
    fontSize: '1.1rem'
  }
});

const Footer = (props) => {
  const { classes } = props;
  return (
    <div className={classNames(classes.footer)} >
      <Grid container
            spacing={0}
            direction="row"
            justify="center"
            alignItems="center"
            className={classNames()}>
        <Grid item
              xl={3} lg={3} md={6} sm={6} xs={12}
              className={classNames()}>
          <Typography
            className={classNames(classes.colTitle)}>
            Footer Col 1
          </Typography>
        </Grid>
        <Grid item
              xl={3} lg={3} md={6} sm={6} xs={12}
              className={classNames()}>
          <Typography
            className={classNames(classes.colTitle)}>
            Footer Col 2
          </Typography>
        </Grid>
        <Grid item
              xl={3} lg={3} md={6} sm={6} xs={12}
              className={classNames()}>
          <Typography
            className={classNames(classes.colTitle)}>
            Footer Col 3
          </Typography>
        </Grid>
        <Grid item
              xl={3} lg={3} md={6} sm={6} xs={12}
              className={classNames()}>
          <Typography
            className={classNames(classes.colTitle)}>
            Footer Col 4
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(Footer));

/*
  direction="row || row-reverse || column || column-reverse"
  justify="flex-start || center || flex-end || space-between || space-around"
  alignItems="flex-start || center || flex-end || stretch || baseline"
*/