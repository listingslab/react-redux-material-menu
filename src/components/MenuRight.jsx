/**
 * Skool
 * Jan 2018
 */

import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import withRoot from '../styles/withRoot';
import classNames from 'classnames';
import MailIcon from 'material-ui-icons/Mail';
import DeleteIcon from 'material-ui-icons/Delete';
import ReportIcon from 'material-ui-icons/Report';
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

const styles = theme => ({
  menuRight: {
  },
});

const MenuRight = (props) => {
  const { classes } = props;
  return (
    <div className={classNames(classes.menuRight)} >
      <ListItem button
                onClick={() => props.closeMenu(props.anchor)}>
        <ListItemIcon>
          <MailIcon />
        </ListItemIcon>
        <ListItemText primary="All mail" />
      </ListItem>
      <ListItem button
                onClick={() => props.closeMenu(props.anchor)}>
        <ListItemIcon>
          <DeleteIcon />
        </ListItemIcon>
        <ListItemText primary="Trash" />
      </ListItem>
      <ListItem button
                onClick={() => props.closeMenu(props.anchor)}>
        <ListItemIcon>
          <ReportIcon />
        </ListItemIcon>
        <ListItemText primary="Spam" />
      </ListItem>
    </div>
  );
}

MenuRight.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(MenuRight));
