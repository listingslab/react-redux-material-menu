/**
 * Skool
 * Jan 2018
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import withRoot from '../styles/withRoot';
import classNames from 'classnames';
import AppBar from 'material-ui/AppBar';
import Layout from './Layout';
import Footer from '../components/Footer';

const constrainAppWidth = 960;

const styles = theme => ({
  app: {
    display: 'flex',
    alignItems: 'flex-start',
    minHeight: '100vh',
    flexDirection: 'column',
  },
  bordered:{
    border: '1px solid white'
  },
  constrainApp:{
    flex: 1,
    maxWidth: constrainAppWidth,
    margin: 'auto'
  },
  footer:{
    width: '100%',
    background: theme.palette.common.appBar,
    minHeight: 56,
  },
  centerFooter:{
    flex: 1,
    maxWidth: constrainAppWidth,
    margin: 'auto'
  },
  fullWidthAppbar:{
    position: 'absolute',
    height: 64,
  },
  [theme.breakpoints.down('xs')]: {
    fullWidthAppbar:{
      height: 56,
    },
  },
});

class App extends Component {

  render() {
    const { classes } = this.props;
    return (
      <div className={classNames(classes.app)}>
        <div className={classNames(classes.constrainApp)}>
          <AppBar className={classNames(classes.fullWidthAppbar)}>
            <div />
          </AppBar>
          <Layout />
        </div>
        <div className={classNames(classes.footer)}>
          <div className={classNames(classes.centerFooter)}>
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withRoot(withStyles(styles)(App));