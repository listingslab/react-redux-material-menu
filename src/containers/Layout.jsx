/**
 * Skool
 * Jan 2018
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {menuOpen, menuClose} from '../redux/actions/app';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Grid from 'material-ui/Grid';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import AccountCircle from 'material-ui-icons/AccountCircle';
import ChevronLeftIcon from 'material-ui-icons/ChevronLeft';
import ChevronRightIcon from 'material-ui-icons/ChevronRight';
import Content from '../components/Content';
import MenuLeft from '../components/MenuLeft';
import MenuRight from '../components/MenuRight';

const drawerWidth = 325;

const styles = theme => ({
  root: {
    width: '100%',
    zIndex: 1,
    overflow: 'hidden',
  },
  titleLink:{
    cursor: 'pointer'
  },
  pullLeft: {
    textAlign: 'left',
  },
  pullMiddle: {
    textAlign: 'center',
  },
  pullRight: {
    textAlign: 'right',
  },
  appFrame: {
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: '100%',
  },
  appBar: {
    boxShadow: 'none',
    position: 'absolute',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'appBarShift-left': {
    marginLeft: drawerWidth,
  },
  'appBarShift-right': {
    marginRight: drawerWidth,
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    height: '100%',
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    height: 'calc(100% - 56px)',
    marginTop: 56,
    [theme.breakpoints.up('sm')]: {
      content: {
        height: 'calc(100% - 64px)',
        marginTop: 64,
      },
    },
  },
  'content-left': {
    marginLeft: -drawerWidth,
  },
  'content-right': {
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'contentShift-left': {
    marginLeft: 0,
  },
  'contentShift-right': {
    marginRight: 0,
  },
});

class Layout extends React.Component {

  constructor (props) {
    super(props);
    this.closeMenu = this.closeMenu.bind(this);
  }

  closeMenu = (anchor) => {
    this.props.dispatchMenuClose(anchor);
  }

  handleMenu = (action, anchor) => {
    switch (action){
      case 'open':
        if (!this.props.store.app.menuLayout.open){
          this.props.dispatchMenuOpen(anchor);
        }else {
          if (anchor === this.props.store.app.menuLayout.anchor) {
            this.props.dispatchMenuClose(anchor);
          }else{
            this.props.dispatchMenuOpen(anchor);
          }
        }
        break;

      case 'close':
        this.props.dispatchMenuClose(anchor);
        break;

      default:
        return;
    }
  }

  render() {
    const { classes } = this.props;
    const { anchor, open } = this.props.store.app.menuLayout;
    const drawer = (
      <Drawer
        type="persistent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor={anchor}
        open={open}
      >
        <div className={classes.drawerInner}>
          <div
            onClick={() => this.handleMenu ('close', anchor)}
            className={classes.drawerHeader}>
            <IconButton>
              { anchor === 'left' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
          </div>
          <Divider />
          { anchor === 'left' ? <MenuLeft closeMenu={this.closeMenu} anchor={anchor} /> : <MenuRight closeMenu={this.closeMenu}  anchor={anchor} /> }
        </div>
      </Drawer>
    );

    let before = null;
    let after = null;

    if (anchor === 'left') {
      before = drawer;
    } else {
      after = drawer;
    }

    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          <AppBar
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open,
              [classes[`appBarShift-${anchor}`]]: open,
            })}
          >
            <Toolbar disableGutters={!open}>

              <Grid
                container
                spacing={16}
                direction="row"
                justify="center"
                alignItems="center"
                className={classNames()}>
                <Grid
                  item
                  xl={4} lg={4} md={4} sm={4} xs={4}
                  className={classNames(classes.pullLeft)}>
                  <IconButton
                    color="contrast"
                    aria-label="open drawer"
                    onClick={() => this.handleMenu ('open', 'left')}
                    className={classNames(classes.menuButton)}
                  >
                    <MenuIcon />
                  </IconButton>
                </Grid>
                <Grid
                  item
                  xl={4} lg={4} md={4} sm={4} xs={4}
                  className={classNames(classes.pullMiddle)}>
                  <Typography
                    onClick={() => this.handleMenu ('close', anchor)}
                    className={classNames(classes.titleLink)}
                    type="title"
                    color="inherit"
                    noWrap>
                    App Title
                  </Typography>
                </Grid>
                <Grid
                  item
                  xl={4} lg={4} md={4} sm={4} xs={4}
                  className={classNames(classes.pullRight)}>
                  <IconButton
                    color="contrast"
                    aria-label="open drawer"
                    onClick={() => this.handleMenu ('open', 'right')}
                    className={classNames(classes.menuButton)}
                  >
                    <AccountCircle />
                  </IconButton>
                </Grid>

              </Grid>
            </Toolbar>
          </AppBar>
          {before}
          <main
            className={classNames(classes.content, classes[`content-${anchor}`])}
          >
            <Content />
          </main>
          {after}
        </div>
      </div>
    );
  }
}

Layout.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = (store) => {
  return {
    store
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchMenuOpen: (anchor) => dispatch(menuOpen(anchor)),
    dispatchMenuClose: (anchor) => dispatch(menuClose(anchor))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(Layout));
