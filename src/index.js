/**
 * Skool
 * Jan 2018
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import { persistStore } from 'redux-persist';
import { configureStore } from './redux/configureStore';
import 'typeface-roboto';
import App from './containers/App';

const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <PersistGate
      loading={null}
      persistor={persistStore(store)}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root'));