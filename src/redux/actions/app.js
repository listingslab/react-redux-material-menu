/**
 * Skool
 * Jan 2018
 */

export function menuOpen(anchor) {

  return {
    type: 'MENU_OPEN',
    anchor
  };
}

export function menuClose(anchor = 'left') {
  // console.log ('action > MENU_CLOSE');
  return {
    type: 'MENU_CLOSE',
    anchor
  };
}
