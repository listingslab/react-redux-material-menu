/**
 * Skool
 * Jan 2018
 */

import initalState from './initalState';
import { createStore, applyMiddleware } from 'redux';
import storage from 'redux-persist/es/storage';
import { persistReducer } from 'redux-persist';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducers from './reducers';

const config = {
  key: 'react-redux-material-menu',
  storage,
}
const reducer = persistReducer(config, reducers);

export function configureStore () {
  return createStore (
    reducer,
    initalState,
    composeWithDevTools (applyMiddleware(thunk)),
  )
};
