/**
 * Skool
 * Jan 2018
 */

import packageJSON from '../../package.json';

const initialState = {
  app: {
    name: packageJSON.name,
    description: packageJSON.description,
    version: packageJSON.version,
    menuLayout:{
      anchor: 'left',
      open: false
    }
  }
}

export default initialState;