/**
 * Skool
 * Jan 2018
 */

export function app (state = false, action) {

  switch (action.type) {

    case 'MENU_OPEN':
      return {
        ...state,
        menuLayout:{
          anchor: action.anchor,
          open: true
        }
      };

    case 'MENU_CLOSE':
      // console.log ('reduce > MENU_CLOSE');
      return {
        ...state,
        menuLayout:{
          anchor: action.anchor,
          open: false
        }
      };

    default:
      return state;
  }
}