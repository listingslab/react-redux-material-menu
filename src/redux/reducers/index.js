/**
 * Skool
 * Jan 2018
 */

import { combineReducers } from 'redux';
import { app } from './app';

export default combineReducers({
  app
});