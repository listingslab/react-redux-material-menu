/**
 * Skool
 * Jan 2018
 */


const background = {
  "default": "#fafafa",
  "paper": "#fff",
  "appBar": "#f5f5f5",
  "contentFrame": "#eeeeee"
}

export default background;
