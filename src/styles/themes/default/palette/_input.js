/**
 * Skool
 * Jan 2018
 */

const input = {
  "bottomLine": "rgba(0, 0, 0, 0.42)",
  "helperText": "rgba(0, 0, 0, 0.54)",
  "labelText": "rgba(0, 0, 0, 0.54)",
  "inputText": "rgba(0, 0, 0, 0.87)",
  "disabled": "rgba(0, 0, 0, 0.42)"
}

export default input;
